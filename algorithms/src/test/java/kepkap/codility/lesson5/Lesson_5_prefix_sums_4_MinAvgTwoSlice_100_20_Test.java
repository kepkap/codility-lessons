package kepkap.codility.lesson5;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.
 * A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice of array A
 * (notice that the slice contains at least two elements).
 * The average of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided by the length of the slice.
 * To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).
 * <p>
 * For example, array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 2
 * A[2] = 2
 * A[3] = 5
 * A[4] = 1
 * A[5] = 5
 * A[6] = 8
 * contains the following example slices:
 * <p>
 * slice (1, 2), whose average is (2 + 2) / 2 = 2;
 * slice (3, 4), whose average is (5 + 1) / 2 = 3;
 * slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.
 * The goal is to find the starting position of a slice whose average is minimal.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers, returns the starting position of the slice with the minimal average.
 * If there is more than one slice with a minimal average, you should return the smallest starting position of such a slice.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 2
 * A[2] = 2
 * A[3] = 5
 * A[4] = 1
 * A[5] = 5
 * A[6] = 8
 * the function should return 1, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [2..100,000];
 * each element of array A is an integer within the range [−10,000..10,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_5_prefix_sums_4_MinAvgTwoSlice_100_20_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{4, 2, 2, 5, 1, 5, 8})).isEqualTo(1);
        assertThat(solution(new int[]{5, 6, 3, 4, 9})).isEqualTo(2);
        assertThat(solution(new int[]{4, 2})).isEqualTo(0);
        assertThat(solution(new int[]{10000, 0, 4, 2, 3, 10000})).isEqualTo(1);
        assertThat(solution(new int[]{10000, 10000, 10000, 10000, 10000, 10000})).isEqualTo(0);
        assertThat(solution(new int[]{-10000, 0, 10000})).isEqualTo(0);
    }

    public int solution(int[] A) {

        float[] prefSum = new float[A.length + 1];

        int result = -1;
        float average = Integer.MAX_VALUE;

        for (int i = 0; i < A.length; i++) {
            prefSum[i + 1] = prefSum[i] + A[i];
        }

        for (int i = 0; i < A.length; i++) {

            for (int j = i + 1; j < A.length; j++) {

                float avg = (prefSum[j + 1] - prefSum[i]) / (j - i + 1);

                if (avg < average) {
                    average = avg;
                    result = i;
                }

            }
        }


        return result;
    }

}
