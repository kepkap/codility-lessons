package kepkap.codility.lesson5;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Write a function:
 * <p>
 * class Solution { public int solution(int A, int B, int K); }
 * <p>
 * that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
 * <p>
 * { i : A ≤ i ≤ B, i mod K = 0 }
 * <p>
 * For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
 * <p>
 * Assume that:
 * <p>
 * A and B are integers within the range [0..2,000,000,000];
 * K is an integer within the range [1..2,000,000,000];
 * A ≤ B.
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(1);
 * expected worst-case space complexity is O(1).
 */
public class Lesson_5_prefix_sums_1_countDiv_100_50_Test {


    @Test
    public void test() {

        assertThat(solution(6, 11, 2)).isEqualTo(3);
        assertThat(solution(7, 11, 2)).isEqualTo(2);
        assertThat(solution(6, 11, 3)).isEqualTo(2);
        assertThat(solution(6, 11, 4)).isEqualTo(1);
        assertThat(solution(1, 2000000000, 4)).isEqualTo(500000000);
    }

    public int solution(int A, int B, int K) {

        int counter = 0;
        int i = A;
        while (i <= B) {

            if (i % K == 0) {
                counter++;
                i += K;
            } else {
                i += 1;
            }
        }


        return counter;
    }


}
