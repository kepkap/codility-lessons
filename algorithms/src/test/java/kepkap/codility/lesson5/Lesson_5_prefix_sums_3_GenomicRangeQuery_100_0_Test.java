package kepkap.codility.lesson5;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A DNA sequence can be represented as a string consisting of the letters A, C, G and T, which correspond to the types of successive nucleotides in the sequence. Each nucleotide has an impact factor, which is an integer. Nucleotides of types A, C, G and T have impact factors of 1, 2, 3 and 4, respectively. You are going to answer several queries of the form: What is the minimal impact factor of nucleotides contained in a particular part of the given DNA sequence?
 * <p>
 * The DNA sequence is given as a non-empty string S = S[0]S[1]...S[N-1] consisting of N characters. There are M queries, which are given in non-empty arrays P and Q, each consisting of M integers. The K-th query (0 ≤ K < M) requires you to find the minimal impact factor of nucleotides contained in the DNA sequence between positions P[K] and Q[K] (inclusive).
 * <p>
 * For example, consider string S = CAGCCTA and arrays P, Q such that:
 * <p>
 * P[0] = 2    Q[0] = 4
 * P[1] = 5    Q[1] = 5
 * P[2] = 0    Q[2] = 6
 * The answers to these M = 3 queries are as follows:
 * <p>
 * The part of the DNA between positions 2 and 4 contains nucleotides G and C (twice), whose impact factors are 3 and 2 respectively, so the answer is 2.
 * The part between positions 5 and 5 contains a single nucleotide T, whose impact factor is 4, so the answer is 4.
 * The part between positions 0 and 6 (the whole string) contains all nucleotides, in particular nucleotide A whose impact factor is 1, so the answer is 1.
 * Write a function:
 * <p>
 * class Solution { public int[] solution(String S, int[] P, int[] Q); }
 * <p>
 * that, given a non-empty zero-indexed string S consisting of N characters and two non-empty zero-indexed arrays P and Q consisting of M integers, returns an array consisting of M integers specifying the consecutive answers to all queries.
 * <p>
 * The sequence should be returned as:
 * <p>
 * a Results structure (in C), or
 * a vector of integers (in C++), or
 * a Results record (in Pascal), or
 * an array of integers (in any other programming language).
 * For example, given the string S = CAGCCTA and arrays P, Q such that:
 * <p>
 * P[0] = 2    Q[0] = 4
 * P[1] = 5    Q[1] = 5
 * P[2] = 0    Q[2] = 6
 * the function should return the values [2, 4, 1], as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * M is an integer within the range [1..50,000];
 * each element of arrays P, Q is an integer within the range [0..N − 1];
 * P[K] ≤ Q[K], where 0 ≤ K < M;
 * string S consists only of upper-case English letters A, C, G, T.
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N+M);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 *
 * https://codility.com/demo/results/trainingDBGSY4-NV6/
 */
public class Lesson_5_prefix_sums_3_GenomicRangeQuery_100_0_Test {


    @Test
    public void test() {

        assertThat(solution("CAGCCTA", new int[]{2, 5, 0}, new int[]{4, 5, 6})).isEqualTo(new int[]{2, 4, 1});
        assertThat(solution("AC", new int[]{0, 0, 1}, new int[]{0, 1, 1})).isEqualTo(new int[]{1, 1, 2});
        assertThat(solution("GGGGGGGGGGGGGGGGGGGGG", new int[]{0}, new int[]{0})).isEqualTo(new int[]{3});
        assertThat(solution("GGGGGGGGGGGGGGGGGGGGG", new int[]{0}, new int[]{10})).isEqualTo(new int[]{3});
    }

    public int[] solution(String S, int[] P, int[] Q) {

        char[] chars = S.toCharArray();

        int[][] prefSums = new int[5][chars.length + 1];


        for (int i = 0; i < chars.length; i++) {

            int a = 0, c = 0, g = 0, t = 0;

            char ch = chars[i];
            if (ch == 'A') {
                a = 1;
            } else if (ch == 'C') {
                c = 1;
            } else if (ch == 'G') {
                g = 1;
            } else if (ch == 'T') {
                t = 1;
            }

            prefSums[1][i + 1] = prefSums[1][i] + a;
            prefSums[2][i + 1] = prefSums[2][i] + c;
            prefSums[3][i + 1] = prefSums[3][i] + g;
            prefSums[4][i + 1] = prefSums[4][i] + t;

        }

        int[] result = new int[P.length];
        for (int i = 0; i < P.length; i++) {

            int from = P[i];
            int to = Q[i] + 1;

            if (prefSums[1][to] - prefSums[1][from] > 0) {
                result[i] = 1;
            } else if (prefSums[2][to] - prefSums[2][from] > 0) {
                result[i] = 2;
            } else if (prefSums[3][to] - prefSums[3][from] > 0) {
                result[i] = 3;
            } else if (prefSums[4][to] - prefSums[4][from] > 0) {
                result[i] = 4;
            }

        }

        return result;
    }

    public int[] solution2(String S, int[] P, int[] Q) {

        char[] chars = S.toCharArray();

        int[] result = new int[P.length];
        for (int i = 0; i < chars.length; i++) {

            for (int j = 0; j < P.length; j++) {

                if (i >= P[j] && i <= Q[j]) {
                    int min = getWeight(chars[i]);
                    if (result[j] == 0 || min < result[j]) {
                        result[j] = min;
                    }
                }

            }
        }

        return result;
    }

    public int[] solution1(String S, int[] P, int[] Q) {

        char[] chars = S.toCharArray();
        int[] pref = new int[chars.length + 1];

        for (int i = 0; i < chars.length; i++) {
            pref[i + 1] = pref[i] + getWeight(chars[i]);
        }


        int[] result = new int[P.length];

        for (int i = 0; i < result.length; i++) {


            int start = P[i];
            int end = Q[i];
            int min;
            min = Math.abs(pref[start + 1] - pref[start]);

            while (start < end) {
                int diff = Math.abs(pref[start + 2] - pref[start + 1]);
                if (diff < min) {
                    min = diff;
                }
                start++;
            }

            result[i] = min;

        }

        return result;
    }

    int getWeight(char a) {
        switch (a) {
            case 'A':
                return 1;
            case 'C':
                return 2;
            case 'G':
                return 3;
            case 'T':
                return 4;
            default:
                throw new IllegalArgumentException("expecting chars [A,C,G,T], got " + a);

        }

    }


}
