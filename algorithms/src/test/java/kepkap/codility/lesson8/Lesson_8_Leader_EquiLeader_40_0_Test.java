package kepkap.codility.lesson8;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.
 * <p>
 * The leader of this array is the value that occurs in more than half of the elements of A.
 * <p>
 * An equi leader is an index S such that 0 ? S < N ? 1 and two sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N ? 1] have leaders of the same value.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 3
 * A[2] = 4
 * A[3] = 4
 * A[4] = 4
 * A[5] = 2
 * we can find two equi leaders:
 * <p>
 * 0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader, whose value is 4.
 * 2, because sequences: (4, 3, 4) and (4, 4, 2) have the same leader, whose value is 4.
 * The goal is to count the number of equi leaders.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers, returns the number of equi leaders.
 * <p>
 * For example, given:
 * <p>
 * A[0] = 4
 * A[1] = 3
 * A[2] = 4
 * A[3] = 4
 * A[4] = 4
 * A[5] = 2
 * the function should return 2, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [?1,000,000,000..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_8_Leader_EquiLeader_40_0_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{4, 3, 4, 4, 4, 2})).isEqualTo(2);
        assertThat(solution(new int[]{0})).isEqualTo(0);
        assertThat(solution(new int[]{0, 1})).isEqualTo(0);
        assertThat(solution(new int[]{0, 1, 0, 0, 1, 0})).isEqualTo(1);
        assertThat(solution(new int[]{0, 0})).isEqualTo(1);
    }

    public int solution(int[] A) {

        if (A.length == 0 || A.length == 1) {
            return 0;
        }

        int count = 0;
        for (int i = 0; i < A.length; i++) {

            int[] left = Arrays.copyOfRange(A, 0, i);
            int[] right = Arrays.copyOfRange(A, i, A.length);
            int leaderL = leader(left);
            int leaderR = leader(right);
            if (leaderL == leaderR) {
                count++;
            }
        }
        return count;
    }

    private int leader(int[] A) {
        if (A.length == 1) {
            return A[0];
        }
        int[] sorted = Arrays.copyOf(A, A.length);
        Arrays.sort(sorted);

        int occurrences = 0 + 1;
        int candidateElement;

        for (int i = 0; i < sorted.length - 1; i++) {
            if (sorted[i] == sorted[i + 1]) {
                candidateElement = sorted[i];
                occurrences++;
                if (occurrences > sorted.length / 2) {
                    return candidateElement;
                }
            }
        }
        return Integer.MIN_VALUE;//means no leader
    }
}

