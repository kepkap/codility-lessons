package kepkap.codility.lesson8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A zero-indexed array A consisting of N integers is given. The dominator of array A is the value that occurs in more than half of the elements of A.
 * <p>
 * For example, consider array A such that
 * <p>
 * A[0] = 3    A[1] = 4    A[2] =  3
 * A[3] = 2    A[4] = 3    A[5] = -1
 * A[6] = 3    A[7] = 3
 * The dominator of A is 3 because it occurs in 5 out of 8 elements of A (namely in those with indices 0, 2, 4, 6 and 7) and 5 is more than a half of 8.
 * <p>
 * Write a function
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns index of any element of array A in which the dominator of A occurs.
 * The function should return ?1 if array A does not have a dominator.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [?2,147,483,648..2,147,483,647].
 * For example, given array A such that
 * <p>
 * A[0] = 3    A[1] = 4    A[2] =  3
 * A[3] = 2    A[4] = 3    A[5] = -1
 * A[6] = 3    A[7] = 3
 * the function may return 0, 2, 4, 6 or 7, as explained above.
 * <p>
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_8_Leader_Dominator_75_100_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{3, 4, 3, 1, 3, -1, 3, 3, 3, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2})).isIn(new Integer[]{-1});
        assertThat(solution(new int[]{3, 4, 3, 2, 3, -1, 3, 3})).isIn(new Integer[]{0, 2, 4, 6, 7});
        assertThat(solution(new int[]{-1, 2, 3, 3, 3, 3, 3, 4})).isIn(new Integer[]{0, 2, 5, 6, 7});
        assertThat(solution(new int[]{3, 4, 2, -1})).isIn(new Integer[]{-1});
        assertThat(solution(new int[]{Integer.MAX_VALUE, 4, Integer.MAX_VALUE, 2, Integer.MAX_VALUE, -1, Integer.MAX_VALUE, 3})).isIn(new Integer[]{0, -1, 2, 4});
        assertThat(solution(new int[]{Integer.MAX_VALUE, 4, Integer.MAX_VALUE, 2, Integer.MAX_VALUE, -1, Integer.MAX_VALUE, 3, Integer.MAX_VALUE})).isIn(new Integer[]{0, 2, 4, 6, 8});
        assertThat(solution(new int[]{10})).isIn(new Integer[]{0});
    }

    public int solution(int[] A) {

        if (A.length == 0) {
            return -1;
        }
        if (A.length == 1) {
            return 0;
        }
        int[] sorted = Arrays.copyOf(A, A.length);


        Arrays.sort(sorted);

        int occurrences = 0 + 1;
        int candidateElement = Integer.MIN_VALUE;

        for (int i = 0; i < sorted.length - 1; i++) {
            if (sorted[i] == sorted[i + 1]) {
                candidateElement = sorted[i];
                occurrences++;
                if (occurrences > sorted.length / 2) {
                    break;
                }
            } else {
                occurrences = 0 + 1;
            }
        }

        if (occurrences <= A.length / 2) {
            return -1;
        }

        for (int i = 0; i < A.length; i++) {
            if (candidateElement == A[i]) {
                return i;
            }

        }

        return -1;
    }
}

