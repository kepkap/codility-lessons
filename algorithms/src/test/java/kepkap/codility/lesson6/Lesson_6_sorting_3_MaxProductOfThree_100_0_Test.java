package kepkap.codility.lesson6;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given. The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).
 * <p>
 * For example, array A such that:
 * <p>
 * A[0] = -3
 * A[1] = 1
 * A[2] = 2
 * A[3] = -2
 * A[4] = 5
 * A[5] = 6
 * contains the following example triplets:
 * <p>
 * (0, 1, 2), product is −3 * 1 * 2 = −6
 * (1, 2, 4), product is 1 * 2 * 5 = 10
 * (2, 4, 5), product is 2 * 5 * 6 = 60
 * Your goal is to find the maximal product of any triplet.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A, returns the value of the maximal product of any triplet.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = -3
 * A[1] = 1
 * A[2] = 2
 * A[3] = -2
 * A[4] = 5
 * A[5] = 6
 * the function should return 60, as the product of triplet (2, 4, 5) is maximal.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [3..100,000];
 * each element of array A is an integer within the range [−1,000..1,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_6_sorting_3_MaxProductOfThree_100_0_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{-3, 1, 2, -2, 5, 6})).isEqualTo(60);
        assertThat(solution(new int[]{-4, -6, 3, 4, 5})).isEqualTo(120);
    }

    public int solution(int[] A) {

//        Arrays.sort(A);

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < A.length - 2; i++) {
            int p = A[i];
            for (int j = i + 1; j < A.length - 1; j++) {
                int q = A[j];
                for (int k = j + 1; k < A.length; k++) {
                    int r = A[k];

                    int tripleProduct = p * q * r;
                    if (tripleProduct > max) {
                        max = tripleProduct;
                    }
                }
            }

        }

        return max;
    }

}

