package kepkap.codility.lesson6;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:
 * <p>
 * A[P] + A[Q] > A[R],
 * A[Q] + A[R] > A[P],
 * A[R] + A[P] > A[Q].
 * For example, consider array A such that:
 * <p>
 * A[0] = 10    A[1] = 2    A[2] = 5
 * A[3] = 1     A[4] = 8    A[5] = 20
 * Triplet (0, 2, 4) is triangular.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns 1 if there exists a triangular triplet for this array and returns 0 otherwise.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 10    A[1] = 2    A[2] = 5
 * A[3] = 1     A[4] = 8    A[5] = 20
 * the function should return 1, as explained above. Given array A such that:
 * <p>
 * A[0] = 10    A[1] = 50    A[2] = 5
 * A[3] = 1
 * the function should return 0.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_6_sorting_2_Triangle_90_100_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{10, 2, 5, 1, 8, 20})).isEqualTo(1);
        assertThat(solution(new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE})).isEqualTo(0);
//        assertThat(solution(new int[]{2, 2, 2})).isEqualTo(0);
        assertThat(solution(new int[]{10, 50, 5, 1})).isEqualTo(0);
    }

    public int solution(int[] A) {

        Arrays.sort(A);


        for (int i = 0; i < A.length - 2; i++) {

            int p = A[i];
            int q = A[i + 1];
            int r = A[i + 2];

            if ((p + q) > r && (q + r) > p && (r + p) > q) {
                return 1;
            }

        }


        return 0;
    }

}

