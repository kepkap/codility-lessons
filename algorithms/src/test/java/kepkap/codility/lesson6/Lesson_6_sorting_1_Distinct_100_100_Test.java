package kepkap.codility.lesson6;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Write a function
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns the number of distinct values in array A.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [−1,000,000..1,000,000].
 * For example, given array A consisting of six elements such that:
 * <p>
 * A[0] = 2    A[1] = 1    A[2] = 1
 * A[3] = 2    A[4] = 3    A[5] = 1
 * the function should return 3, because there are 3 distinct values appearing in array A, namely 1, 2 and 3.
 * <p>
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_6_sorting_1_Distinct_100_100_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{2, 1, 1, 2, 3, 1})).isEqualTo(3);
    }

    public int solution(int[] A) {

        Arrays.sort(A);

        int counter = 0;

        int el = Integer.MIN_VALUE;
        for (int i = 0; i < A.length; i++) {

            if (el != A[i]) {
                counter++;
                el = A[i];
            }
        }

        return counter;
    }

}

