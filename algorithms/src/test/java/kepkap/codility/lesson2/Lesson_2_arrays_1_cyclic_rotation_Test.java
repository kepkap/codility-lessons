package kepkap.codility.lesson2;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A zero-indexed array A consisting of N integers is given. Rotation of the array means that each element is shifted right by one index,
 * and the last element of the array is also moved to the first place.
 * <p>
 * For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7].
 * The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int[] solution(int[] A, int K); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.
 * <p>
 * For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].
 * <p>
 * Assume that:
 * <p>
 * N and K are integers within the range [0..100];
 * each element of array A is an integer within the range [?1,000..1,000].
 * In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_2_arrays_1_cyclic_rotation_Test {


    @Test
    public void test() {


        int[] input = {3, 8, 9, 7, 6};
        int[] expected = {9, 7, 6, 3, 8};
//        int[] solution = solution(input, 3);
//        System.out.println("input   : " + Arrays.toString(input));
//        System.out.println("expected: " + Arrays.toString(expected));
//        System.out.println("solution: " + Arrays.toString(solution));
//        assertThat(solution).isEqualTo(expected);
        assertThat(solution(input, 3)).isEqualTo(expected);
        assertThat(solution(new int[]{}, 4)).isEqualTo(new int[]{});
        assertThat(solution(new int[]{1}, 4)).isEqualTo(new int[]{1});
        assertThat(solution(new int[]{1, 2, 3}, 3)).isEqualTo(new int[]{1, 2, 3});
        assertThat(solution(new int[]{1, 2, 3}, -3)).isEqualTo(new int[]{1, 2, 3});
        assertThat(solution(new int[]{-1, -2, -3}, 2)).isEqualTo(new int[]{-2, -3, -1});
        assertThat(solution(new int[]{1, 1, 2, 3, 5}, 42)).isEqualTo(new int[]{3, 5, 1, 1, 2});
    }

    public int[] solution(int[] A, int K) {

        if (A.length == 0) {
            return A;
        }

        if (K < 0) {
            return A;
        }

        for (int j = 0; j < K; j++) {

            shiftRight(A);
        }
        return A;
    }

    private void shiftRight(int[] A) {

        if (A.length == 0) {
            return;
        }

        int preserved = A[0];
        for (int i = 0; i < A.length - 1; i++) {
            int swap = preserved;
            preserved = A[i + 1];
            A[i + 1] = swap;
        }
        A[0] = preserved;
    }

    public int[] solutionzz(int[] A, int K) {

        int[] res = new int[A.length];

        if (A.length == 0) {
            return res;
        }
        if (A.length < K) {
            return A;
        }

        int i = 0;
        int k = K;
        while (k > 0) {
            res[i] = A[A.length - k];
            k--;
            i++;
        }

        for (int j = 0; j < A.length - K; j++) {
            res[i] = A[j];
            i++;
        }


        return res;
    }
}
