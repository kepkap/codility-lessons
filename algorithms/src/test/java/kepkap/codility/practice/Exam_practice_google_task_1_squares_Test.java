package kepkap.codility.practice;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Write a function public int whole_cubes_count ( int A,int B ) where it should return whole cubes within the range
 * <p>
 * For example if A=8 and B=65, all the possible cubes in the range are 2^3 =8 , 3^3 =27 and 4^3=64, so the function should return count 3
 * A and B are integers within the range [-10,000..10,000]; b. A <= B
 */
public class Exam_practice_google_task_1_squares_Test {


    @Test
    public void test() {

        assertThat(solution(8, 65)).isEqualTo(3);
        assertThat(solution(8, 63)).isEqualTo(2);
        assertThat(solution(8, 125)).isEqualTo(4);
        assertThat(solution(-10000, 10000)).isEqualTo(43);
        assertThat(solution(-10000, 0)).isEqualTo(22);
        assertThat(solution(0, 10000)).isEqualTo(22);
        assertThat(solution(4, 27)).isEqualTo(2);
    }

    public int solution(int A, int B) {


        int counter = 0;

        int x = (int) Math.ceil(Math.cbrt((double) A));

        int cube = x * x * x;
        while (cube >= A && cube <= B) {
            counter++;
            x++;
            cube = x * x * x;
        }

        return counter;
    }


}

