package kepkap.codility.lesson12;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Two positive integers N and M are given. Integer N represents the number of chocolates arranged in a circle, numbered from 0 to N − 1.
 * <p>
 * You start to eat the chocolates. After eating a chocolate you leave only a wrapper.
 * <p>
 * You begin with eating chocolate number 0. Then you omit the next M − 1 chocolates or wrappers on the circle, and eat the following one.
 * <p>
 * More precisely, if you ate chocolate number X, then you will next eat the chocolate with number (X + M) modulo N (remainder of division).
 * <p>
 * You stop eating when you encounter an empty wrapper.
 * <p>
 * For example, given integers N = 10 and M = 4. You will eat the following chocolates: 0, 4, 8, 2, 6.
 * <p>
 * The goal is to count the number of chocolates that you will eat, following the above rules.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int N, int M); }
 * <p>
 * that, given two positive integers N and M, returns the number of chocolates that you will eat.
 * <p>
 * For example, given integers N = 10 and M = 4. the function should return 5, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N and M are integers within the range [1..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(log(N+M));
 * expected worst-case space complexity is O(log(N+M)).
 */
public class Lesson_12_Euclidean_1_ChokolatesByNumbers_100_0_Test {


    @Test
    public void test() {


        assertThat(solution(10, 4)).isEqualTo(5);
        assertThat(solution(947853, 4453)).isEqualTo(482256);
        //TODO performance failed with OOM Error https://codility.com/demo/results/training2XKDNP-NUE/
    }

    public int solution(int N, int M) {

        boolean[] eaten = new boolean[N];

        int counter = 0;
        int i = 0;

        while (0 <= i && !eaten[i % N]) {
            eaten[i % N] = true;
            counter++;
            i += M;
        }
        return counter;
    }

    public int solution_from_inet_also_100_0(int N, int M) {
        int lcm = N * M / gcd(N, M);
        // Least common multiple
        return lcm / M;
        //N / gcd(N, M)
    }

    private int gcd(int a, int b) {
        // Get the greatest common divisor
        if (a % b == 0) {
            return b;
        } else {
            return gcd(b, a % b);
        }
    }


}

