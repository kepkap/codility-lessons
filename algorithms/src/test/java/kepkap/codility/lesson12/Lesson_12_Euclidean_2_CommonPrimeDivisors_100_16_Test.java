package kepkap.codility.lesson12;

import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A prime is a positive integer X that has exactly two distinct divisors: 1 and X. The first few prime integers are 2, 3, 5, 7, 11 and 13.
 * <p>
 * A prime D is called a prime divisor of a positive integer P if there exists a positive integer K such that D * K = P. For example, 2 and 5 are prime divisors of 20.
 * <p>
 * You are given two positive integers N and M. The goal is to check whether the sets of prime divisors of integers N and M are exactly the same.
 * <p>
 * For example, given:
 * <p>
 * N = 15 and M = 75, the prime divisors are the same: {3, 5};
 * N = 10 and M = 30, the prime divisors aren't the same: {2, 5} is not equal to {2, 3, 5};
 * N = 9 and M = 5, the prime divisors aren't the same: {3} is not equal to {5}.
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A, int[] B); }
 * <p>
 * that, given two non-empty zero-indexed arrays A and B of Z integers, returns the number of positions K for which the prime divisors of A[K] and B[K] are exactly the same.
 * <p>
 * For example, given:
 * <p>
 * A[0] = 15   B[0] = 75
 * A[1] = 10   B[1] = 30
 * A[2] = 3    B[2] = 5
 * the function should return 1, because only one pair (15, 75) has the same set of prime divisors.
 * <p>
 * Assume that:
 * <p>
 * Z is an integer within the range [1..6,000];
 * each element of arrays A, B is an integer within the range [1..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(Z*log(max(A)+max(B))2);
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_12_Euclidean_2_CommonPrimeDivisors_100_16_Test {


    @Test
    public void test() {


        assertThat(solution(new int[]{15, 10, 3}, new int[]{75, 30, 5})).isEqualTo(1);
        //https://codility.com/demo/results/training9GVUJG-JV3/
    }

    public int solution(int[] A, int[] B) {

        int N = A.length;
        int counter = 0;
        for (int i = 0; i < N; i++) {
            if (primeDivisors(A[i]).equals(primeDivisors(B[i]))) {
                counter++;
            }
        }
        return counter;
    }

    private Set<Integer> primeDivisors(int n) {
        int i = 1;
        Set<Integer> divisors = new HashSet<>();
        while (i <= n) {
            if (n % i == 0 && prime(i)) {
                divisors.add(i);
            }
            i += 1;
        }
        return divisors;
    }

    private boolean prime(int n) {
        int i = 2;

        while (i * i <= n) {
            if (n % i == 0) {
                return false;
            }
            i += 1;
        }
        return true;
    }

    private int gcd(int a, int b) {
        if (a % b == 0) {
            return b;
        } else {
            return gcd(b, a % b);
        }
    }

}

