package kepkap.codility.lesson10;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * An integer N is given, representing the area of some rectangle.
 * <p>
 * The area of a rectangle whose sides are of length A and B is A * B, and the perimeter is 2 * (A + B).
 * <p>
 * The goal is to find the minimal perimeter of any rectangle whose area equals N. The sides of this rectangle should be only integers.
 * <p>
 * For example, given integer N = 30, rectangles of area 30 are:
 * <p>
 * (1, 30), with a perimeter of 62,
 * (2, 15), with a perimeter of 34,
 * (3, 10), with a perimeter of 26,
 * (5, 6), with a perimeter of 22.
 * Write a function:
 * <p>
 * class Solution { public int solution(int N); }
 * <p>
 * that, given an integer N, returns the minimal perimeter of any rectangle whose area is exactly equal to N.
 * <p>
 * For example, given an integer N = 30, the function should return 22, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(sqrt(N));
 * expected worst-case space complexity is O(1).
 */
public class Lesson_10_Prime_and_Composite_2_MinPerimeterRectangle_60_80_Test {


    @Test
    public void test() {

        assertThat(solution(30)).isEqualTo(22);
        assertThat(solution(1)).isEqualTo(4);
        assertThat(solution(100000000)).isEqualTo(40000);
    }

    public int solution(int N) {

        int minPerimeter = Integer.MAX_VALUE;
        int i = 1;

        while (i * i <= N) {

            if (N % i == 0) {
                int perimeter = 2 * (i + N / i);

                minPerimeter = Math.min(minPerimeter, perimeter);
            }

            i++;
        }


        return minPerimeter;
    }


}

