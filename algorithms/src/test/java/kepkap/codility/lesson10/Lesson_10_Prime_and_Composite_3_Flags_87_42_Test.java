package kepkap.codility.lesson10;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.
 * <p>
 * A peak is an array element which is larger than its neighbours. More precisely, it is an index P such that 0 < P < N − 1 and A[P − 1] < A[P] > A[P + 1].
 * <p>
 * For example, the following array A:
 * <p>
 * A[0] = 1
 * A[1] = 5
 * A[2] = 3
 * A[3] = 4
 * A[4] = 3
 * A[5] = 4
 * A[6] = 1
 * A[7] = 2
 * A[8] = 3
 * A[9] = 4
 * A[10] = 6
 * A[11] = 2
 * has exactly four peaks: elements 1, 3, 5 and 10.
 * <p>
 * You are going on a trip to a range of mountains whose relative heights are represented by array A, as shown in a figure below.
 * You have to choose how many flags you should take with you. The goal is to set the maximum number of flags on the peaks, according to certain rules.
 * <p>
 * <p>
 * <p>
 * Flags can only be set on peaks. What's more, if you take K flags, then the distance between any two flags should be greater than or equal to K.
 * The distance between indices P and Q is the absolute value |P − Q|.
 * <p>
 * For example, given the mountain range represented by array A, above, with N = 12, if you take:
 * <p>
 * two flags, you can set them on peaks 1 and 5;
 * three flags, you can set them on peaks 1, 5 and 10;
 * four flags, you can set only three flags, on peaks 1, 5 and 10.
 * You can therefore set a maximum of three flags in this case.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A of N integers, returns the maximum number of flags that can be set on the peaks of the array.
 * <p>
 * For example, the following array A:
 * <p>
 * A[0] = 1
 * A[1] = 5
 * A[2] = 3
 * A[3] = 4
 * A[4] = 3
 * A[5] = 4
 * A[6] = 1
 * A[7] = 2
 * A[8] = 3
 * A[9] = 4
 * A[10] = 6
 * A[11] = 2
 * the function should return 3, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..400,000];
 * each element of array A is an integer within the range [0..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_10_Prime_and_Composite_3_Flags_87_42_Test {


    @Test
    public void test() {

        assertThat(solution(new int[]{1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2})).isEqualTo(3);
        assertThat(solution(new int[]{1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2, 4, 5, 2})).isEqualTo(0);
    }

    public int solution(int[] A) {

        int N = A.length;

        if (N < 2) {
            return 0;
        }

        boolean[] peaks = new boolean[N];
        for (int i = 1; i < A.length - 1; i++) {
            if (A[i - 1] < A[i] && A[i] > A[i + 1]) {
                peaks[i] = true;
            }
        }

        for (int i = 1; i < A.length; i++) {
            boolean check = check(i, peaks);
            if (check) {
                return i;
            }
        }


        return 0;
    }

    private boolean check(int x, boolean[] peaks) {

        int i = 0;
        int count = 0;
        while (i < peaks.length) {

            if (peaks[i]) {
                i += x;
                count += 1;
            } else {
                i++;
            }

        }
        return count == x;
    }


}

