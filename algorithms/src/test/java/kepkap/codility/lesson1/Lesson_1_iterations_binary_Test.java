package kepkap.codility.lesson1;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at both ends in the binary representation of N.
 * For example, number 9 has binary representation 1001 and contains a binary gap of length 2.
 * The number 529 has binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3.
 * The number 20 has binary representation 10100 and contains one binary gap of length 1.
 * The number 15 has binary representation 1111 and has no binary gaps.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int N); }
 * <p>
 * that, given a positive integer N, returns the length of its longest binary gap. The function should return 0 if N doesn't contain a binary gap.
 * <p>
 * For example, given N = 1041 the function should return 5, because N has binary representation 10000010001 and so its longest binary gap is of length 5.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(log(N));
 * expected worst-case space complexity is O(1).
 * <p>
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_1_iterations_binary_Test {


    @Test
    public void test() {

        int num = 1041;
        System.out.println(num + "\t-[10] = " + Arrays.toString(toBinary(num)) + "-[2]");
        assertThat(solutionMy(num)).isEqualTo(5);
        assertThat(solutionRuslan(num)).isEqualTo(5);

        num = 1033232332;
        System.out.println(num + "\t-[10] = " + Arrays.toString(toBinary(num)) + "-[2]");
        assertThat(solutionMy(num)).isEqualTo(2);

        assertThat(solutionRuslan(num)).isEqualTo(2);
    }

    public int solutionRuslan(int N) {
        if (N <= 0) {
            throw new IllegalArgumentException("N has to be an integer within the range [1..2,147,483,647]");
        }

        int result = 0;
        int currentZerosCount = 0;

        while (N != 0) {
            if ((N & 1) == 1) {
                if (currentZerosCount - 1 > result) {
                    result = currentZerosCount - 1;
                }
                currentZerosCount = 1;
            } else if (currentZerosCount > 0) {
                ++currentZerosCount;
            }

            N >>>= 1;
        }
        return result;
    }

    public int solutionMy(int N) {

        int[] binary = toBinary(N);

        int start = -1;
        int end = -1;

        int count = 0;
        int result = count;

        for (int i = binary.length - 1; i > 0; i--) {

            if (start == -1 && binary[i] == 1) {
                start = i;
            }

            if (start != -1 && binary[i - 1] == 0) {
                count += 1;
            }


            if (binary[i - 1] == 1) {
                end = i;
                start = -1;
                if (count > result) {
                    result = count;
                }
                count = 0;
            }
        }

        return result;
    }

    private int[] toBinary(Integer num) {

        int[] binary = new int[32];
        int i = binary.length - 1;
        while (num >= 1 & i >= 0) {
            int remainder = num % 2;
            num = num / 2;
            binary[i] = remainder;
            i -= 1;
        }
        return binary;
    }

}
