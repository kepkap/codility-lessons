package kepkap.codility.lesson3;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A zero-indexed array A consisting of N different integers is given.
 * The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
 * <p>
 * Your goal is to find that missing element.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A, returns the value of the missing element.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 2
 * A[1] = 3
 * A[2] = 1
 * A[3] = 5
 * the function should return 4, as it is the missing element.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * the elements of A are all distinct;
 * each element of array A is an integer within the range [1..(N + 1)].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_3_time_complexity_3_perm_missing_elem_Test {


    @Test
    public void test() {
        int[] A = new int[4];
        A[0] = 2;
        A[1] = 3;
        A[2] = 1;
        A[3] = 5;

        assertThat(4).isEqualTo(solution(A));
        assertThat(10).isEqualTo(solution(new int[]{9, 1, 8, 2, 7, 3, 6, 4, 5, 11}));
    }

    public int solution(int[] A) {

        int[] counts = new int[A.length + 2];

        for (int i = 0; i < A.length; i++) {
            counts[A[i]] = 1;
        }

        for (int i = 1; i < counts.length; i++) {
            if (counts[i] == 0) {
                return i;
            }

        }

        return -1;

    }

    public int solution_w_sort(int[] A) {

        Arrays.sort(A);

        for (int i = 0; i < A.length; i++) {
            if (A[i + 1] - A[i] > 1) {
                return A[i] + 1;
            }
        }

        return -1;

    }

    private void sort(int[] a) {

        for (int i = 0; i < a.length; i++) {
            int i1 = a[i];

        }

    }


}
