package kepkap.codility.lesson13;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The Fibonacci sequence is defined using the following recursive formula:
 * <p>
 * F(0) = 0
 * F(1) = 1
 * F(M) = F(M - 1) + F(M - 2) if M >= 2
 * A small frog wants to get to the other side of a river.
 * The frog is initially located at one bank of the river (position ?1) and wants to get to the other bank (position N).
 * The frog can jump over any distance F(K), where F(K) is the K-th Fibonacci number.
 * Luckily, there are many leaves on the river, and the frog can jump between the leaves, but only in the direction of the bank at position N.
 * <p>
 * The leaves on the river are represented in a zero-indexed array A consisting of N integers.
 * Consecutive elements of array A represent consecutive positions from 0 to N ? 1 on the river. Array A contains only 0s and/or 1s:
 * <p>
 * 0 represents a position without a leaf;
 * 1 represents a position containing a leaf.
 * The goal is to count the minimum number of jumps in which the frog can get to the other side of the river (from position ?1 to position N).
 * The frog can jump between positions ?1 and N (the banks of the river) and every position containing a leaf.
 * <p>
 * For example, consider array A such that:
 * <p>
 * A[0] = 0
 * A[1] = 0
 * A[2] = 0
 * A[3] = 1
 * A[4] = 1
 * A[5] = 0
 * A[6] = 1
 * A[7] = 0
 * A[8] = 0
 * A[9] = 0
 * A[10] = 0
 * The frog can make three jumps of length F(5) = 5, F(3) = 2 and F(5) = 5.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns the minimum number of jumps by which the frog can get to the other side of the river.
 * If the frog cannot reach the other side of the river, the function should return ?1.
 * <p>
 * For example, given:
 * <p>
 * A[0] = 0
 * A[1] = 0
 * A[2] = 0
 * A[3] = 1
 * A[4] = 1
 * A[5] = 0
 * A[6] = 1
 * A[7] = 0
 * A[8] = 0
 * A[9] = 0
 * A[10] = 0
 * the function should return 3, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer that can have one of the following values: 0, 1.
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 *
 *
 *
 * http://www.martinkysel.com/codility-ladder-solution/
 */
public class Lesson_13_Fibonacci_1_FibFrog_0_0_Test {


    @Test
    public void test() {


        assertThat(solution(new int[]{0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0})).isEqualTo(3);
        assertThat(solution(new int[]{0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1})).isEqualTo(2);
        assertThat(solution(new int[]{0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0})).isEqualTo(3);
        assertThat(solution(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})).isEqualTo(-1);

        //todo https://codility.com/demo/results/training2S3MWD-6PS/

        assertThat(solution(new int[]{})).isEqualTo(1);
        assertThat(solution(new int[]{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0})).isEqualTo(2);
    }

    public int solution(int[] A) {

        if (A.length == 0) {
            return 1;
        }



        int count = 0;
        int from = 0;
        int lastLeave = 0;

        int i = 1;
        while (i < A.length) {

            if (A[i - 1] == 1 && A[i] == 0) {
                lastLeave = i - 1;
                int j = 2;
                int f = fib(2);
                int distance = i - from;
                while (f <= distance) {
                    f = fib(j++);
                }
                from = i;
                count++;
            }

            if (i == A.length - 1) {
                int j = 2;
                int f = fib(2);
                int distance = i - from;
                while (f <= distance) {
                    f = fib(j++);
                }
                from = i;
                count++;
            }

            i++;
        }

        if (lastLeave == 0) {
            return -1;
        }

        return count;
    }

    private int fib(int N) {
        if (N <= 1) {
            return N;
        }
        return fib(N - 1) + fib(N - 2);
    }


}

