package kepkap.codility.study;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_1_Test {


    private int[] numbers = new int[]{0, 4, 56, 23, 5, 6, 7, 5, 3, 2, 23, 4, 56, 7, 7};

    private int findX(int[] numbers, int k) {
        int x = -1;

        int matchesCount = 0;
        int mismatchesCount = 0;

        for (int i = 1; i < numbers.length; i++) {

            mismatchesCount = 0;
            if (numbers[i - 1] == k) {
                matchesCount++;
                mismatchesCount = 0;
                x= -1;
                System.out.println(String.format("match, numbers[%s]=%s", i - 1, numbers[i - 1]));
            }
            for (int j = i + 1; j < numbers.length; j++) {

                if (numbers[j] != k) {
                    mismatchesCount++;
                    System.out.println(String.format("mismatch, numbers[%s]=%s", j, numbers[j]));
                }

                if (mismatchesCount == matchesCount) {
                    System.out.println(String.format("x=%s", i));
                    x = i;
                    break;
                }
            }

        }
        return x;
    }

    @Test
    public void test_4() {
        assertThat(findX(numbers, 4)).isEqualTo(12);
    }

    @Test
    public void test_5() {
        assertThat(findX(numbers, 5)).isEqualTo(12);
    }

    @Test
    public void test_56() {
        assertThat(findX(numbers, 56)).isEqualTo(-1);
    }

}
