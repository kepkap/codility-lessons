package kepkap.codility.study;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_3_equi_Test {


    private int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 7};
    private int[] B = new int[]{-1, 3, -4, 5, 1, -6, 2, 1, 6};
//    private int[] B = new int[]{1, 3, 4, 5, 3};


    @Test(enabled = false)
    public void test_A() {

        System.out.println("source--- " + Arrays.toString(A));
        System.out.println("pref_sum- " + Arrays.toString(calcPref(A)));
        System.out.println("suf_sum-- " + Arrays.toString(calcSuf(A)));

        assertThat(solution(A)).isEqualTo(7);
    }

    @Test
    public void test_B() {

        System.out.println("source--- " + Arrays.toString(B));
        System.out.println("pref_sum- " + Arrays.toString(calcPref(B)));
        System.out.println("suf_sum-- " + Arrays.toString(calcSuf(B)));

        assertThat(solution(B)).isEqualTo(4);
    }

    public int solution(int[] a) {
        int result = -1;

        int[] pref = calcPref(a);
        int[] suff = calcSuf(a);

        for (int i = 1; i < a.length; i++) {

            if (pref[i - 1] == suff[suff.length - i]) {
                System.out.println(pref[i]);
                System.out.println(i);
                return i;
            }
        }

        return result;
    }

    private int[] calcPref(int[] a) {
        int[] result = new int[a.length];

        for (int i = 0; i < a.length; i++) {
            if (i == 0) {
                result[i] = a[i];
            } else {
                result[i] = result[i - 1] + a[i];
            }
        }
        return result;
    }

    private int[] calcSuf(int[] a) {
        int[] result = new int[a.length];
        for (int i = a.length - 1; i >= 0; i--) {
            if (i == a.length - 1) {
                result[i] = a[i];
            } else {
                result[i] = result[i + 1] + a[i];
            }
        }
        return result;
    }

    private int[] calcSuf_1(int[] a) {
        int[] result = new int[a.length];
        for (int i = a.length - 1; i >= 0; i--) {
            int j = a.length - 1 - i;
            if (j == 0) {
                result[j] = a[i];
            } else {
                result[j] = result[j - 1] + a[i];
            }
        }
        return result;
    }

}
