package kepkap.codility.study;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_14_binary_search_boards_Test {


    @Test(enabled = false)
    public void test() {
        int[] roof = {0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};//15

        assertThat(boardsCount(roof, 8)).isEqualTo(1);
        assertThat(boardsCount(roof, 7)).isEqualTo(2);
        assertThat(boardsCount(roof, 9)).isEqualTo(1);
        assertThat(boardsCount(roof, 3)).isEqualTo(3);
        assertThat(boardsCount(roof, 2)).isEqualTo(4);
        //may be not optimal
        assertThat(boardsCount(roof, 7)).isEqualTo(2);

        //optimal
        assertThat(optBoards(roof, 4)).isEqualTo(2);
//        assertThat(optBoards(roof, 6)).isEqualTo(2);
        assertThat(optBoards(roof, 8)).isEqualTo(1);
    }


    private int optBoards(int[] roof, int boardCount) {

        int result = -1;

        int beg = 0;
        int end = roof.length;

        while (beg <= end) {
            int mid = (beg + end) / 2;

            if (boardsCount(roof, mid) <= boardCount) {
                end = mid - 1;
                result = mid;
            } else {
                beg = mid + 1;
            }
        }
        return result;
    }


    private int boardsCount(int[] roof, int boardLength) {

        int boards = 0;
        int last = -1;

        for (int i = 0; i < roof.length; i++) {
            if (roof[i] == 1 && last < i) {
                boards += 1;
                last = i + boardLength - 1;
            }
        }
        return boards;
    }


}
