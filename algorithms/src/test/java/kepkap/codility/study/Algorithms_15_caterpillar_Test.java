package kepkap.codility.study;

import org.testng.annotations.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_15_caterpillar_Test {


    @Test
    public void test() {
        int[] A = {6, 2, 7, 4, 1, 3, 6};

        assertThat(new Integer[]{7, 4, 1}).isEqualTo(caterpillar(A, 12));
        assertThat(new Integer[]{6, 2, 7}).isEqualTo(caterpillar(A, 15));
    }


    private Integer[] caterpillar(int[] A, int s) {

        int front = 0;
        int total = 0;

        List<Integer> res = new LinkedList<>();

        for (int back = 0; back < A.length; back++) {

            while (front < A.length && total + A[front] <= s) {

                total += A[front];
                res.add(A[front]);

                front += 1;

                if (total == s) {
                    return res.toArray(new Integer[res.size()]);
                }

            }

            total -= A[back];
            res.removeAll(Collections.singleton(A[back]));


        }


        return new Integer[]{};
    }


}
