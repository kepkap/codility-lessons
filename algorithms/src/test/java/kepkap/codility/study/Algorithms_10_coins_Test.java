package kepkap.codility.study;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_10_coins_Test {


    private int[] coins = new int[11];

    {
        for (int i = 0; i < coins.length; i++) {
            coins[i] = 0;
        }
    }


    @Test
    public void test() {
        int n = 10;
        int result = 0;
        for (int i = 1; i < n + 1; i++) {

            int k = i;

            while (k <= n) {

                coins[k] = (coins[k] + 1) % 2;

                k += i;
            }
            result += coins[i];


        }


        assertThat(result).isEqualTo(3);
    }


}
