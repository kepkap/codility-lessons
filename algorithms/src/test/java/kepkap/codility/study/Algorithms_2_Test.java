package kepkap.codility.study;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_2_Test {


    @Test
    public void test_less() {
        assertThat(12 % 12).isEqualTo(0);
        assertThat(11 % 12).isEqualTo(11);
        assertThat(10 % 12).isEqualTo(10);
        assertThat(7 % 12).isEqualTo(7);
        assertThat(1 % 12).isEqualTo(1);
        assertThat(0 % 12).isEqualTo(0);
    }

    @Test
    public void test_more() {

        assertThat(13 % 12).isEqualTo(1);
        assertThat(14 % 12).isEqualTo(2);
        assertThat(15 % 12).isEqualTo(3);
        assertThat(16 % 12).isEqualTo(4);
        assertThat(17 % 12).isEqualTo(5);
        assertThat(18 % 12).isEqualTo(6);
        assertThat(19 % 12).isEqualTo(7);
        assertThat(20 % 12).isEqualTo(8);
        assertThat(21 % 12).isEqualTo(9);
        assertThat(22 % 12).isEqualTo(10);
        assertThat(23 % 12).isEqualTo(11);
        assertThat(24 % 12).isEqualTo(0);
        assertThat(50 % 12).isEqualTo(2);
        assertThat(75 % 12).isEqualTo(3);
        assertThat(100 % 12).isEqualTo(4);
    }


}
