package kepkap.codility.study;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 13.01.2016.
 */
public class Algorithms_17_frog_dynamic_Test {


    public int[] frog(int[] jumpLength, int distance) {

        int[] dp = new int[distance];

        dp[0] = 1;

        for (int i = 1; i < distance; i++) {
            dp[i] = 0;
        }


        for (int j = 0; j < jumpLength.length; j++) {

            for (int i = 1; i < distance; i++) {
                if (i % jumpLength[j] == 0) {
                    dp[i] = dp[i] + 1;
                }
            }

        }
        return dp;
    }

    @Test
    public void test_frog() {


        assertThat(frog(new int[]{1, 3}, 10)).isEqualTo(new int[]{1, 1, 1, 2, 1, 1, 2, 1, 1, 2});
        assertThat(frog(new int[]{1, 3, 5}, 10)).isEqualTo(new int[]{1, 1, 1, 2, 1, 2, 2, 1, 1, 2});
    }


}
