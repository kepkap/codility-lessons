package kepkap.codility.exam1;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

/**

 */
public class Exam_1_Task_2_Test {


    @Test
    public void test() {

        assertThat(solution(new int[]{1, 0, 0, 1, 1})).isEqualTo(new int[]{1, 1, 0, 1});//9 -9
        assertThat(solution(new int[]{1, 0, 0, 1, 1, 1})).isEqualTo(new int[]{1, 1, 0, 1, 0, 1, 1});//23 -23

        //from Ruslan
        assertArrayEquals(new int[] {0}, solution(new int[] {0})); // 0 -> 0
    }

    public int[] solution(int[] A) {
        return negate(A);
    }


    private int[] negate(int[] binary) {

        binary = fillMostSignificantZero(binary);
        int i = 0;
        while (i < binary.length - 1) {

            //-9
            //1,1,0,1
            //9
            //1,0,0,1,1
            //-23
            // 1, 0, 0, 1, 1, 1
            //23
            // 1, 1, 0, 1, 0, 1, 1

            //-128, 64, -32, 16, -8, 4, -2, 1
            if (binary[i] == 1 && binary[i + 1] == 1) {//[1,0] negated is [1,1]
                binary[i] = 1;
                binary[i + 1] = 0;
                i++;
            } else if (binary[i] == 1 && binary[i + 1] == 0) {//[1,1] negated is [1,0]
                binary[i] = 1;
                binary[i + 1] = 1;
                i++;
            } else {
                binary[i] = binary[i];
            }
            i++;
        }

        return removeMostSignificantZeros(binary);
    }

    private int[] removeMostSignificantZeros(int[] binary) {

        while (binary[binary.length - 1] == 0) {
            int[] trunkated = new int[binary.length - 1];

            for (int j = 0; j < trunkated.length; j++) {
                trunkated[j] = binary[j];
            }
            binary = trunkated;
        }
        return binary;
    }

    private int[] fillMostSignificantZero(int[] binary) {

        if (binary[binary.length - 1] == 1) {
            int[] extended = new int[binary.length + 1];

            for (int i = 0; i < binary.length; i++) {
                extended[i] = binary[i];
            }
            return extended;

        }
        return binary;
    }

    private int toDec(int[] A) {
        int d = 0;
        for (int i = 0; i < A.length; i++) {

            if (i == 0) {
                d += A[i];
                continue;
            }
            int sign = i % 2 == 0 ? 1 : -1;
            int pow = i - 1;
            int p = 2 << pow;
            d += (A[i] * p * sign);
        }
        return d;
    }

    private int[] toBin_WRONG(Integer d) {

        int[] binary = new int[16];
        int i = binary.length - 1;
        while (Math.abs(d) >= 1 & i >= 0) {
            int remainder = d % (-2);
            d = d / (-2);
            binary[i] = remainder;
            i -= 1;
        }
        return binary;
    }


}

