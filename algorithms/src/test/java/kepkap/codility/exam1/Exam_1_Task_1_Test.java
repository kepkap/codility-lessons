package kepkap.codility.exam1;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.AssertJUnit.assertEquals;

/**

 */
public class Exam_1_Task_1_Test {


    @Test
    public void test() {
        assertThat(solution(5, new int[]{1, 2, 5, 5, 2, 3, 5})).isEqualTo(4);

        assertThat(solution(5, new int[]{5, 5, 1, 7, 2, 3, 5})).isEqualTo(4);
        assertThat(solution(7, new int[]{1, 2, 5, 5, 2, 3, 5})).isEqualTo(-1);
        assertThat(solution(5, new int[]{5, 5, 3, 2})).isEqualTo(2);
        assertThat(solution(5, new int[]{1, 1, 1, 1, 1, 1, 1, 1})).isEqualTo(-1);
        assertThat(solution(1, new int[]{1, 1, 1, 1, 1, 1, 1, 1})).isEqualTo(-1);
        assertThat(solution(1, new int[]{1, 1, 1, 1, 2, 2, 2, 1})).isEqualTo(3);
        assertThat(solution(5, new int[]{5, 5, 3})).isEqualTo(-1);


        assertThat(solution_0_0(5, new int[]{5, 5, 1, 7, 2, 3, 5})).isEqualTo(4);
        assertThat(solution_0_0(7, new int[]{1, 2, 5, 5, 2, 3, 5})).isEqualTo(4);
        assertThat(solution_0_0(5, new int[]{5, 5, 3, 2})).isEqualTo(-1);

        //from Ruslan
        assertEquals(0, solution(5, new int[]{5, 5, 5, 5, 5, 5, 5}));
        assertEquals(7, solution(5, new int[]{1, 1, 1, 1, 1, 1, 1}));
        assertEquals(1, solution(5, new int[]{5, 5, 5, 5, 5, 5, 1}));
        assertEquals(6, solution(5, new int[]{5, 1, 1, 1, 1, 1, 1}));
        assertEquals(1, solution(5, new int[]{5, 1, 5}));
    }

    public int solution(int X, int[] A) {

        int N = A.length;
        int[] matches = new int[N];
        int[] mismatches = new int[N];

        for (int i = 0; i < N; i++) {

            if (A[i] == X) {
                matches[i] = 1;
            } else {
                mismatches[i] = 1;
            }
        }

        int[] prefMatches = new int[N + 1];

        for (int i = 0; i < N; i++) {
            prefMatches[i + 1] = prefMatches[i] + matches[i];
        }

        int[] suffMismatches = new int[N + 1];

        for (int i = N - 1; i >= 0; i--) {
            if (i == N - 1) {
                suffMismatches[i] = mismatches[i];
            }
            suffMismatches[i] = suffMismatches[i + 1] + mismatches[i];
        }

        for (int i = suffMismatches.length - 1; i > 0; i--) {

            if (suffMismatches[i - 1] == prefMatches[i]) {
                return i - 1;
            }
        }

        return -1;
    }


    public int solution_0_0(int X, int[] A) {

        int countL = 0;
        int countR = 0;


        int fromL = 0;
        int fromR = A.length - 1;
        while (fromL < fromR) {
            if (A[fromL] == X) {
                countL++;
            }
            fromL++;
            if (fromL == fromR) {
                continue;
            }
            if (A[fromR] != X) {
                countR++;
            }
            fromR--;
        }

        if (countL == countR) {
            return A.length - fromR;
        }
        return -1;
    }


}

