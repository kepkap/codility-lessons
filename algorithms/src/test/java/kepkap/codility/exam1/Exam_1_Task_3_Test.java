package kepkap.codility.exam1;

import org.testng.annotations.Test;

import static java.lang.Math.abs;
import static org.assertj.core.api.Assertions.assertThat;

/**

 */
public class Exam_1_Task_3_Test {


    @Test
    public void test() {
        assertThat(solution(1, 3)).isEqualTo(-1);
        assertThat(solution(4, 5)).isEqualTo(3);
        assertThat(solution(-4, 5)).isEqualTo(3);
        assertThat(solution(4, -5)).isEqualTo(3);
        assertThat(solution(-4, -5)).isEqualTo(3);
        assertThat(solution(0, 0)).isEqualTo(-1);
        assertThat(solution(1, 1)).isEqualTo(-1);
        assertThat(solution(1, 2)).isEqualTo(1);
        assertThat(solution(0, 8)).isEqualTo(4);
        assertThat(solution(8, 0)).isEqualTo(4);
        assertThat(solution(-8, 0)).isEqualTo(4);
        assertThat(solution(0, -8)).isEqualTo(4);
        assertThat(solution(Integer.MAX_VALUE, Integer.MAX_VALUE)).isEqualTo(-2);
        assertThat(solution(-Integer.MAX_VALUE, -Integer.MAX_VALUE)).isEqualTo(-2);

    }

    public int solution(int A, int B) {

        if (abs(A) < 2 && abs(B) < 2) {
            return -1;
        }


        int moves = 0;
        int[] position = new int[]{0, 0};
        while (!(exceeded(position, A, B) || reached(position, A, B))) {

            if (abs(A) - abs(position[0]) < abs(B) - abs(position[1])) {
                position = moveMoreB(position, A >= position[0], B >= position[1]);
            } else {
                position = moveMoreA(position, A >= position[0], B >= position[1]);
            }
            moves++;

            if (moves > 100000000) {
                return -2;
            }
        }

        return reached(position, A, B) ? moves : -1;
    }

    private int[] moveMoreB(int[] from, boolean positiveA, boolean positiveB) {
        if (positiveA) {
            from[0] += 1;
        } else {
            from[0] -= 1;
        }

        if (positiveB) {
            from[1] += 2;
        } else {
            from[1] -= 2;
        }
        return from;
    }

    private int[] moveMoreA(int[] from, boolean positiveA, boolean positiveB) {
        if (positiveA) {
            from[0] += 2;
        } else {
            from[0] -= 2;
        }

        if (positiveB) {
            from[1] += 1;
        } else {
            from[1] -= 1;
        }
        return from;
    }

    private boolean reached(int[] position, int A, int B) {
        return position[0] == A && position[1] == B;
    }

    private boolean exceeded(int[] position, int A, int B) {
        return abs(position[0]) >= abs(A) && abs(position[1]) >= abs(B);
    }


}

