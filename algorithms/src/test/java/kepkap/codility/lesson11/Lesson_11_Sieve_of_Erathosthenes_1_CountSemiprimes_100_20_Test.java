package kepkap.codility.lesson11;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A prime is a positive integer X that has exactly two distinct divisors: 1 and X. The first few prime integers are 2, 3, 5, 7, 11 and 13.
 * <p>
 * A semiprime is a natural number that is the product of two (not necessarily distinct) prime numbers.
 * The first few semiprimes are 4, 6, 9, 10, 14, 15, 21, 22, 25, 26.
 * <p>
 * You are given two non-empty zero-indexed arrays P and Q, each consisting of M integers. These arrays represent queries about the number of semiprimes within specified ranges.
 * <p>
 * Query K requires you to find the number of semiprimes within the range (P[K], Q[K]), where 1 ≤ P[K] ≤ Q[K] ≤ N.
 * <p>
 * For example, consider an integer N = 26 and arrays P, Q such that:
 * <p>
 * P[0] = 1    Q[0] = 26
 * P[1] = 4    Q[1] = 10
 * P[2] = 16   Q[2] = 20
 * The number of semiprimes within each of these ranges is as follows:
 * <p>
 * (1, 26) is 10,
 * (4, 10) is 4,
 * (16, 20) is 0.
 * Write a function:
 * <p>
 * class Solution { public int[] solution(int N, int[] P, int[] Q); }
 * <p>
 * that, given an integer N and two non-empty zero-indexed arrays P and Q consisting of M integers,
 * returns an array consisting of M elements specifying the consecutive answers to all the queries.
 * <p>
 * For example, given an integer N = 26 and arrays P, Q such that:
 * <p>
 * P[0] = 1    Q[0] = 26
 * P[1] = 4    Q[1] = 10
 * P[2] = 16   Q[2] = 20
 * the function should return the values [10, 4, 0], as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..50,000];
 * M is an integer within the range [1..30,000];
 * each element of arrays P, Q is an integer within the range [1..N];
 * P[i] ≤ Q[i].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(log(N))+M);
 * expected worst-case space complexity is O(N+M), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_11_Sieve_of_Erathosthenes_1_CountSemiprimes_100_20_Test {


    @Test
    public void test() {

        assertThat(solution(26, new int[]{1, 4, 16}, new int[]{26, 10, 20})).isEqualTo(new int[]{10, 4, 0});
        assertThat(solution(1, new int[]{1}, new int[]{1})).isEqualTo(new int[]{0});
    }

    public int[] solution(int N, int[] P, int[] Q) {

//        if (P.length < 2 || Q.length < 2) {
//            return new int[]{N};
//        }

        int[] numbers = new int[N + 1];
        boolean[] primes = new boolean[N + 1];
        for (int i = 2; i < primes.length; i++) {
            numbers[i] = i;
            primes[i] = true;
        }

        int i = 2;

        while (i * i <= N) {
            if (primes[i]) {
                int k = i * i;
                while (k < N) {
                    primes[k] = false;
                    k += i;
                }
            }
            i += 1;
        }

        boolean[] sprimes = new boolean[N + 1];
        int p = 0;
        for (int j = 0; j < numbers.length; j++) {

            for (int k = j + 1; k < numbers.length; k++) {
                if (primes[j] && primes[k]) {
                    int sprime = numbers[j] * numbers[k];
                    if (sprime <= N) {
                        sprimes[sprime] = true;
                    }
                }
            }

            if (primes[j]) {
                int sprime = numbers[j] * numbers[j];
                if (sprime <= N) {
                    sprimes[sprime] = true;
                }
            }
        }


        int[] result = new int[P.length];

        for (int ii = 0; ii < result.length; ii++) {
            for (int jj = P[ii]; jj <= Q[ii]; jj++) {
                if (sprimes[jj]) {
                    result[ii] += 1;
                }
            }
        }

        return result;
    }


}

