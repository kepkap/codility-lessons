package kepkap.codility.lesson11;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * You are given a non-empty zero-indexed array A consisting of N integers.
 * <p>
 * For each number A[i] such that 0 ≤ i < N, we want to count the number of elements of the array that are not the divisors of A[i]. We say that these elements are non-divisors.
 * <p>
 * For example, consider integer N = 5 and array A such that:
 * <p>
 * A[0] = 3
 * A[1] = 1
 * A[2] = 2
 * A[3] = 3
 * A[4] = 6
 * For the following elements:
 * <p>
 * A[0] = 3, the non-divisors are: 2, 6,
 * A[1] = 1, the non-divisors are: 3, 2, 3, 6,
 * A[2] = 2, the non-divisors are: 3, 3, 6,
 * A[3] = 3, the non-divisors are: 2, 6,
 * A[6] = 6, there aren't any non-divisors.
 * Write a function:
 * <p>
 * class Solution { public int[] solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers,
 * returns a sequence of integers representing the amount of non-divisors.
 * <p>
 * The sequence should be returned as:
 * <p>
 * a structure Results (in C), or
 * a vector of integers (in C++), or
 * a record Results (in Pascal), or
 * an array of integers (in any other programming language).
 * For example, given:
 * <p>
 * A[0] = 3
 * A[1] = 1
 * A[2] = 2
 * A[3] = 3
 * A[4] = 6
 * the function should return [2, 4, 3, 2, 0], as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..50,000];
 * each element of array A is an integer within the range [1..2 * N].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_11_Sieve_of_Erathosthenes_2_CountNonDivisible_100_25_Test {


    @Test
    public void test() {

        assertThat(solution(new int[]{3, 1, 2, 3, 6})).isEqualTo(new int[]{2, 4, 3, 2, 0});
        assertThat(solution(new int[]{3, 4})).isEqualTo(new int[]{1, 1});
    }

    public int[] solution(int[] A) {


        int[] nonDivisors = new int[A.length * 2 + 1];

        for (int i = 0; i < A.length; i++) {

            boolean repeated = nonDivisors[A[i]] != 0;
            for (int j = 0; j < A.length; j++) {

                if (!repeated && i != j && A[i] % A[j] != 0) {
                    nonDivisors[A[i]] += 1;
                }
            }
        }

        int[] result = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            result[i] = nonDivisors[A[i]];
        }

        return result;
    }


}

