package kepkap.codility.lesson14;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * You are given integers K, M and a non-empty zero-indexed array A consisting of N integers.
 * Every element of the array is not greater than M.
 * <p>
 * You should divide this array into K blocks of consecutive elements.
 * The size of the block is any integer between 0 and N. Every element of the array should belong to some block.
 * <p>
 * The sum of the block from X to Y equals A[X] + A[X + 1] + ... + A[Y]. The sum of empty block equals 0.
 * <p>
 * The large sum is the maximal sum of any block.
 * <p>
 * For example, you are given integers K = 3, M = 5 and array A such that:
 * <p>
 * A[0] = 2
 * A[1] = 1
 * A[2] = 5
 * A[3] = 1
 * A[4] = 2
 * A[5] = 2
 * A[6] = 2
 * The array can be divided, for example, into the following blocks:
 * <p>
 * [2, 1, 5, 1, 2, 2, 2], [], [] with a large sum of 15;
 * [2], [1, 5, 1, 2], [2, 2] with a large sum of 9;
 * [2, 1, 5], [], [1, 2, 2, 2] with a large sum of 8;
 * [2, 1], [5, 1], [2, 2, 2] with a large sum of 6.
 * The goal is to minimize the large sum. In the above example, 6 is the minimal large sum.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int K, int M, int[] A); }
 * <p>
 * that, given integers K, M and a non-empty zero-indexed array A consisting of N integers, returns the minimal large sum.
 * <p>
 * For example, given K = 3, M = 5 and array A such that:
 * <p>
 * A[0] = 2
 * A[1] = 1
 * A[2] = 5
 * A[3] = 1
 * A[4] = 2
 * A[5] = 2
 * A[6] = 2
 * the function should return 6, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N and K are integers within the range [1..100,000];
 * M is an integer within the range [0..10,000];
 * each element of array A is an integer within the range [0..M].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N+M));
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_14_Binary_Search_1_MinMaxDivision_CHEATED_Test {


    @Test
    public void test() {
        assertThat(solution(3, 5, new int[]{2, 1, 5, 1, 2, 2, 2})).isEqualTo(6);
    }

    @Test
    public void test_bin_search() {
        assertThat(binSearch(new int[]{1, 1, 1, 2, 2, 2, 5}, 5)).isEqualTo(6);
        assertThat(binSearch(new int[]{2, 1, 5, 1, 2, 2, 2}, 10)).isEqualTo(-1);
    }

    public int solution(int K, int M, int[] A) {
        return binarySearch(A, K, M);
    }

    private boolean blockSizeIsValid(int[] A, int K, int maxBlockSize) {
        int blockSum = 0;
        int blockCnt = 0;

        for (int i = 0; i < A.length; i++) {
            if (blockSum + A[i] > maxBlockSize) {
                blockSum = A[i];
                blockCnt += 1;
            } else {
                blockSum += A[i];
            }
            if (blockCnt >= K) {
                return false;
            }
        }
        return true;
    }

    private int binarySearch(int[] A, int K, int using_M_will_give_you_wrong_results) {
        int lowerBound = 0;
        int upperBound = 0;
        for (int i = 0; i < A.length; i++) {
            lowerBound = Math.max(lowerBound, A[i]);
            upperBound += A[i];
        }

        if (K == 1) {
            return upperBound;
        }
        if (K >= A.length) {
            return lowerBound;
        }

        while (lowerBound <= upperBound) {

            int candidateMid = (lowerBound + upperBound) / 2;
            if (blockSizeIsValid(A, K, candidateMid)) {
                upperBound = candidateMid - 1;
            } else {
                lowerBound = candidateMid + 1;
            }
        }
        return lowerBound;
    }


    private int binSearch(int[] A, int x) {
        Arrays.sort(A);
        int beg = 0;
        int end = A.length - 1;
        while (beg <= end) {

            int mid = (beg + end) / 2;

            if(A[mid] == x) {
                return mid;
            }

            if (A[mid] < x) {
                beg = mid + 1;
            } else {
                end = mid - 1;
            }

        }
        return -1;
    }
}

