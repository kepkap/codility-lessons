package kepkap.codility.lesson7;

import org.testng.annotations.Test;

import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A string S consisting of N characters is called properly nested if:
 * <p>
 * S is empty;
 * S has the form "(U)" where U is a properly nested string;
 * S has the form "VW" where V and W are properly nested strings.
 * For example, string "(()(())())" is properly nested but string "())" isn't.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(String S); }
 * <p>
 * that, given a string S consisting of N characters, returns 1 if string S is properly nested and 0 otherwise.
 * <p>
 * For example, given S = "(()(())())", the function should return 1 and given S = "())", the function should return 0, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..1,000,000];
 * string S consists only of the characters "(" and/or ")".
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(1) (not counting the storage required for input arguments).
 */
public class Lesson_7_Stacks_and_Queues_3_Nesting_Test {


    @Test
    public void test() {
        assertThat(solution("")).isEqualTo(1);
        assertThat(solution("(U)")).isEqualTo(1);
        assertThat(solution("(U)(B)")).isEqualTo(1);
        assertThat(solution("((U)(B))(C)")).isEqualTo(1);
        assertThat(solution("(()(())())")).isEqualTo(1);
        assertThat(solution("(()")).isEqualTo(0);
        assertThat(solution("(((")).isEqualTo(0);
        assertThat(solution("()))))")).isEqualTo(0);
        assertThat(solution(")))")).isEqualTo(0);
        assertThat(solution("())")).isEqualTo(0);
    }

    public int solution(String S) {

        char[] chars = S.toCharArray();

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < chars.length; i++) {
            char aChar = chars[i];

            switch (aChar) {
                case '(':
                    stack.push(aChar);
                    break;
                case ')':
                    if (stack.isEmpty() || stack.peek() != '(') {
                        return 0;
                    }
                    stack.pop();
                default:
                    break;
            }
        }

        return stack.isEmpty() ? 1 : 0;
    }

}

