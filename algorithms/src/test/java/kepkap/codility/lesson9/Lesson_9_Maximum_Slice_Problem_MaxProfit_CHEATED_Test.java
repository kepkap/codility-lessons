package kepkap.codility.lesson9;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A zero-indexed array A consisting of N integers is given. It contains daily prices of a stock share for a period of N consecutive days.
 * If a single share was bought on day P and sold on day Q, where 0 ≤ P ≤ Q < N, then the profit of such transaction is equal to A[Q] − A[P], provided that A[Q] ≥ A[P].
 * Otherwise, the transaction brings loss of A[P] − A[Q].
 * <p>
 * For example, consider the following array A consisting of six elements such that:
 * <p>
 * A[0] = 23171
 * A[1] = 21011
 * A[2] = 21123
 * A[3] = 21366
 * A[4] = 21013
 * A[5] = 21367
 * If a share was bought on day 0 and sold on day 2, a loss of 2048 would occur because A[2] − A[0] = 21123 − 23171 = −2048.
 * If a share was bought on day 4 and sold on day 5, a profit of 354 would occur because A[5] − A[4] = 21367 − 21013 = 354.
 * Maximum possible profit was 356. It would occur if a share was bought on day 1 and sold on day 5.
 * <p>
 * Write a function,
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers containing daily prices of a stock share for a period of N consecutive days,
 * returns the maximum possible profit from one transaction during this period.
 * The function should return 0 if it was impossible to gain any profit.
 * <p>
 * For example, given array A consisting of six elements such that:
 * <p>
 * A[0] = 23171
 * A[1] = 21011
 * A[2] = 21123
 * A[3] = 21366
 * A[4] = 21013
 * A[5] = 21367
 * the function should return 356, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..400,000];
 * each element of array A is an integer within the range [0..200,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_9_Maximum_Slice_Problem_MaxProfit_CHEATED_Test {


    @Test
    public void test() {

        assertThat(solution(new int[]{23171, 21011, 21123, 21366, 21013, 21367})).isEqualTo(356);
        assertThat(solution(new int[]{1, 2, 3, 4, 5})).isEqualTo(4);
        assertThat(solution(new int[]{1, 1, 5, 2, 3})).isEqualTo(4);
        assertThat(solution(new int[]{1, 1, 5, 2, 3, 6, 7, 3, 2})).isEqualTo(6);
    }

    public int solution(int[] A) {

        if (A.length == 1 || A.length == 0) {
            return 0;
        }

        int N = A.length;

        int minPrice = A[0];
        int maxEndingHere = 0;
        int maxSoFar = 0;

        for (int i = 0; i < N; i++) {

            minPrice = Math.min(minPrice, A[i]);
            maxEndingHere = Math.max(maxEndingHere, A[i] - minPrice);

            maxSoFar = Math.max(maxEndingHere, maxSoFar);//???
        }

        return maxSoFar;
    }


}

