package kepkap.codility.lesson9;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.
 * <p>
 * A triplet (X, Y, Z), such that 0 ≤ X < Y < Z < N, is called a double slice.
 * <p>
 * The sum of double slice (X, Y, Z) is the total of A[X + 1] + A[X + 2] + ... + A[Y − 1] + A[Y + 1] + A[Y + 2] + ... + A[Z − 1].
 * <p>
 * For example, array A such that:
 * <p>
 * A[0] = 3
 * A[1] = 2
 * A[2] = 6
 * A[3] = -1
 * A[4] = 4
 * A[5] = 5
 * A[6] = -1
 * A[7] = 2
 * contains the following example double slices:
 * <p>
 * double slice (0, 3, 6), sum is 2 + 6 + 4 + 5 = 17,
 * double slice (0, 3, 7), sum is 2 + 6 + 4 + 5 − 1 = 16,
 * double slice (3, 4, 5), sum is 0.
 * The goal is to find the maximal sum of any double slice.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers, returns the maximal sum of any double slice.
 * <p>
 * For example, given:
 * <p>
 * A[0] = 3
 * A[1] = 2
 * A[2] = 6
 * A[3] = -1
 * A[4] = 4
 * A[5] = 5
 * A[6] = -1
 * A[7] = 2
 * the function should return 17, because no double slice of array A has a sum of greater than 17.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [3..100,000];
 * each element of array A is an integer within the range [−10,000..10,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_9_Maximum_Slice_Problem_MaxDoubleSliceSum_CHEATED_Test {


    @Test
    public void test() {

        /* A triplet (X, Y, Z), such that 0 ≤ X < Y < Z < N, is called a double slice.
        * The sum of double slice (X, Y, Z) is the total of A[X + 1] + A[X + 2] + ... + A[Y − 1] + A[Y + 1] + A[Y + 2] + ... + A[Z − 1].
        */
        assertThat(solution(new int[]{3, 2, 6, -1, 4, 5, -1, 2})).isEqualTo(17);
    }

    public int solution(int[] A) {


        int[] maxL = new int[A.length];
        int[] maxR = new int[A.length];

        for (int i = 1; i < A.length - 1; i++) {
            maxL[i] = Math.max(maxL[i - 1] + A[i], 0);
        }

        for (int i = A.length - 2; i > 0; i--) {
            maxR[i] = Math.max(maxR[i + 1] + A[i], 0);
        }

        int result = 0;

        for (int i = 1; i < A.length - 1; i++) {
            result = Math.max(result, maxL[i - 1] + maxR[i + 1]);

        }

        return result;
    }


}

