package kepkap.codility.lesson4;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A of N integers, returns the minimal positive integer (greater than 0) that does not occur in A.
 * <p>
 * For example, given:
 * <p>
 * A[0] = 1
 * A[1] = 3
 * A[2] = 6
 * A[3] = 4
 * A[4] = 1
 * A[5] = 2
 * the function should return 5.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * <p>
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_4_counting_elements_3_missing_integer_Test {


    @Test
    public void test() {

        assertThat(solution(new int[]{1, 3, 6, 4, 1, 2})).isEqualTo(5);
        assertThat(solution(new int[]{-1, -3, -6, -4, -1, -2})).isEqualTo(1);
        assertThat(solution(new int[]{-1, 3, -6, 4, 1, -2})).isEqualTo(2);
        assertThat(solution(new int[]{1})).isEqualTo(2);
        assertThat(solution(new int[]{2})).isEqualTo(3);
        assertThat(solution(new int[]{-1, -2, -3, -4, -5, -6})).isEqualTo(1);
        assertThat(solution(new int[]{-1})).isEqualTo(1);
        assertThat(solution(new int[]{0})).isEqualTo(1);
        assertThat(solution(new int[]{-1, -2, -3, -4, -5, 1})).isEqualTo(2);
        assertThat(solution(new int[]{-1, 0})).isEqualTo(1);
        assertThat(solution(new int[]{-1, 0, 1})).isEqualTo(2);


        int[] seq = new int[100001];
        for (int i = 0; i < seq.length; i++) {
            seq[i] = i + 1;
        }
        assertThat(solution(seq)).isEqualTo(100001);

    }

    public int solution(int[] A) {

        if (A.length == 1) {
            return A[0] > 0 ? A[0] + 1 : 1;
        }


        boolean[] positivesPresent = new boolean[A.length + 1];


        for (int i = 0; i < A.length; i++) {

            if (A[i] > 0 && A[i] < A.length) {
                positivesPresent[A[i]] = true;
            }
        }

        for (int i = 1; i < positivesPresent.length; i++) {
            if (!positivesPresent[i]) {
                return i;
            }
        }
        //means we have all numbers present (sequence), so return last one  + 1;
        return positivesPresent.length + 1;//just to satisfy shifted sequence test 1,2,...100,000, with expected 100,001
    }

    public int solution2(int[] A) {
        if (A.length == 0) {
            throw new IllegalArgumentException("array size must be [1..100,000], got 0");
        }

        Arrays.sort(A);


        if (A.length == 1) {
            return A[0] > 0 ? A[0] + 1 : 1;
        }

        for (int i = 0; i < A.length; i++) {

            if (A[i] <= 0) {
                continue;
            }

            if (i == A.length - 1) {
                return Math.min(0, A[i]) + 1;
            }

            int diff = A[i + 1] - A[i];

            if (diff > 1) {
                return A[i] + 1;
            }
        }
        return 1;
    }


}
