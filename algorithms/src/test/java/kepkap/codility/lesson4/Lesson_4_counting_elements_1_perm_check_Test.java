package kepkap.codility.lesson4;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N integers is given.
 * <p>
 * A permutation is a sequence containing each element from 1 to N once, and only once.
 * <p>
 * For example, array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 1
 * A[2] = 3
 * A[3] = 2
 * is a permutation, but array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 1
 * A[2] = 3
 * is not a permutation, because value 2 is missing.
 * <p>
 * The goal is to check whether array A is a permutation.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A, returns 1 if array A is a permutation and 0 if it is not.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 1
 * A[2] = 3
 * A[3] = 2
 * the function should return 1.
 * <p>
 * Given array A such that:
 * <p>
 * A[0] = 4
 * A[1] = 1
 * A[2] = 3
 * the function should return 0.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [1..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_4_counting_elements_1_perm_check_Test {


    @Test
    public void test() {
        int[] A = new int[4];
        A[0] = 4;
        A[1] = 1;
        A[2] = 3;
        A[3] = 2;

        assertThat(1).isEqualTo(solution(new int[]{4, 1, 3, 2}));
        assertThat(0).isEqualTo(solution(new int[]{4, 1, 3}));
        assertThat(1).isEqualTo(solution(new int[]{1}));
        assertThat(1).isEqualTo(solution(new int[]{1000000000}));
        assertThat(0).isEqualTo(solution(new int[]{400, 10, 30}));
        assertThat(0).isEqualTo(solution(new int[]{4, 1, 3, 1002}));
    }

    public int solution(int[] A) {

        Arrays.sort(A);

        for (int i = 0; i < A.length - 1; i++) {

            if (A[i + 1] - A[i] > 1) {
                return 0;
            }

        }
        return 1;
    }

    //Alex Fedorov
    public int solution_AF(int[] A) {

        int[] elementsCounter = new int[A.length + 1];

        int arraySum = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] > A.length || A[i] <= 0) {
                return 0;
            }

            elementsCounter[A[i]] += 1;
            arraySum += elementsCounter[A[i]];

            if (elementsCounter[A[i]] > 1) {
                return 0;
            }
        }

        return (arraySum < A.length) ? 0 : 1;
    }

    public int solution_wrong_attempt_without_sorting(int[] A) {


        //element = 1..1000000000
        int[] count = new int[1000 + 1];

        for (int i = 0; i < A.length; i++) {
            count[A[i] % 1000] += 1;
        }

        for (int i = 2; i < count.length; i++) {
            if (count[i] == 1 && count[i - 1] == 0) {
                return 0;
            }
        }
        return 1;
    }

}
