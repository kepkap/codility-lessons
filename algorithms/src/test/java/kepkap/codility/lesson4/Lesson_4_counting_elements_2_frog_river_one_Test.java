package kepkap.codility.lesson4;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A small frog wants to get to the other side of a river. The frog is currently located at position 0, and wants to get to position X. Leaves fall from a tree onto the surface of the river.
 * <p>
 * You are given a non-empty zero-indexed array A consisting of N integers representing the falling leaves. A[K] represents the position where one leaf falls at time K, measured in seconds.
 * <p>
 * The goal is to find the earliest time when the frog can jump to the other side of the river. The frog can cross only when leaves appear at every position across the river from 1 to X. You may assume that the speed of the current in the river is negligibly small, i.e. the leaves do not change their positions once they fall in the river.
 * <p>
 * For example, you are given integer X = 5 and array A such that:
 * <p>
 * A[0] = 1
 * A[1] = 3
 * A[2] = 1
 * A[3] = 4
 * A[4] = 2
 * A[5] = 3
 * A[6] = 5
 * A[7] = 4
 * In second 6, a leaf falls into position 5. This is the earliest time when leaves appear in every position across the river.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int X, int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers and integer X, returns the earliest time when the frog can jump to the other side of the river.
 * <p>
 * If the frog is never able to jump to the other side of the river, the function should return ?1.
 * <p>
 * For example, given X = 5 and array A such that:
 * <p>
 * A[0] = 1
 * A[1] = 3
 * A[2] = 1
 * A[3] = 4
 * A[4] = 2
 * A[5] = 3
 * A[6] = 5
 * A[7] = 4
 * the function should return 6, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N and X are integers within the range [1..100,000];
 * each element of array A is an integer within the range [1..X].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(X), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_4_counting_elements_2_frog_river_one_Test {


    @Test
    public void test() {

        assertThat(6).isEqualTo(solution(5, new int[]{1, 3, 1, 4, 2, 3, 5, 4}));
        assertThat(4).isEqualTo(solution(3, new int[]{1, 3, 1, 4, 2, 3, 5, 4}));
        assertThat(-1).isEqualTo(solution(5, new int[]{1, 2, 3}));
        assertThat(-1).isEqualTo(solution(5, new int[]{1, 2, 3, 4}));
        assertThat(-1).isEqualTo(solution(2, new int[]{1, 1, 1, 1}));
        assertThat(-1).isEqualTo(solution(5, new int[]{1}));
        assertThat(-1).isEqualTo(solution(5, new int[]{1, 3, 3}));
        assertThat(0).isEqualTo(solution(1, new int[]{1, 1, 1}));
        assertThat(0).isEqualTo(solution(1, new int[]{1}));
        assertThat(-1).isEqualTo(solution(2, new int[]{2, 2, 2, 2, 2}));
    }

    public int solution(int X, int[] A) {

        if (A.length == 0) {
            return -1;
        }

        int leavesCount = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] > leavesCount) {
                leavesCount = A[i];
            }
        }

        if (X > leavesCount) {
            return -1;
        }

        int[] leaveDropTimes = new int[leavesCount + 1];

        for (int i = 0; i < leaveDropTimes.length; i++) {
            leaveDropTimes[i] = -1;
        }

        for (int i = 0; i < A.length; i++) {
            if (leaveDropTimes[A[i]] != -1) {
                continue;
            }
            leaveDropTimes[A[i]] = i;
        }


        int earliestTime = -1;

        int i = 1;
        while (i <= X) {
            if (leaveDropTimes[i] == -1) {
                return -1;
            }
            if (leaveDropTimes[i] > earliestTime) {
                earliestTime = leaveDropTimes[i];
            }
            i++;
        }

        return earliestTime;
    }


    public int solutionzz(int X, int[] A) {

        int[] counts = new int[A.length + 1];

        for (int i = 0; i < counts.length; i++) {
            counts[i] = -1;
        }

        for (int i = 0; i < A.length; i++) {
            if (counts[A[i]] == -1) {
                counts[A[i]] = i;
            }
        }

        int i = 1;
        int max = -1;
        while (i <= X && i < counts.length) {

            if (counts[i] == -1) {
                return -1;
            }

            if (max < counts[i]) {
                max = counts[i];
            }
            i++;
        }

        return max;
    }

}
