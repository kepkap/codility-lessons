package kepkap.codility.lesson4;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * You are given N counters, initially set to 0, and you have two possible operations on them:
 * <p>
 * increase(X) − counter X is increased by 1,
 * max counter − all counters are set to the maximum value of any counter.
 * A non-empty zero-indexed array A of M integers is given. This array represents consecutive operations:
 * <p>
 * if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
 * if A[K] = N + 1 then operation K is max counter.
 * For example, given integer N = 5 and array A such that:
 * <p>
 * A[0] = 3
 * A[1] = 4
 * A[2] = 4
 * A[3] = 6
 * A[4] = 1
 * A[5] = 4
 * A[6] = 4
 * the values of the counters after each consecutive operation will be:
 * <p>
 * (0, 0, 1, 0, 0)
 * (0, 0, 1, 1, 0)
 * (0, 0, 1, 2, 0)
 * (2, 2, 2, 2, 2)
 * (3, 2, 2, 2, 2)
 * (3, 2, 2, 3, 2)
 * (3, 2, 2, 4, 2)
 * The goal is to calculate the value of every counter after all operations.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int[] solution(int N, int[] A); }
 * <p>
 * that, given an integer N and a non-empty zero-indexed array A consisting of M integers, returns a sequence of integers representing the values of the counters.
 * <p>
 * The sequence should be returned as:
 * <p>
 * a structure Results (in C), or
 * a vector of integers (in C++), or
 * a record Results (in Pascal), or
 * an array of integers (in any other programming language).
 * For example, given:
 * <p>
 * A[0] = 3
 * A[1] = 4
 * A[2] = 4
 * A[3] = 6
 * A[4] = 1
 * A[5] = 4
 * A[6] = 4
 * the function should return [3, 2, 2, 4, 2], as explained above.
 * <p>
 * Assume that:
 * <p>
 * N and M are integers within the range [1..100,000];
 * each element of array A is an integer within the range [1..N + 1].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N+M);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * <p>
 * Created by Denis Kuchugurov
 * on 21.01.2016.
 */
public class Lesson_4_counting_elements_4_max_counters_100_60_Test {


    @Test
    public void test() {

        int[] A = new int[7];

        A[0] = 3;
        A[1] = 4;
        A[2] = 4;
        A[3] = 6;
        A[4] = 1;
        A[5] = 4;
        A[6] = 4;

        assertThat(solution(5, A)).isEqualTo(new int[]{3, 2, 2, 4, 2});
        assertThat(solution(5, new int[]{6, 6, 6, 6, 6, 6, 6})).isEqualTo(new int[]{0, 0, 0, 0, 0});

    }

    public int[] solution(int N, int[] A) {
        int[] counters = new int[N];

        int maxCounter = 0;

        for (int i = 0; i < A.length; i++) {

            if (A[i] == N + 1) {
                for (int j = 0; j < counters.length; j++) {
                    counters[j] = maxCounter;
                }
            } else {
                counters[A[i] - 1] += 1;

                if (maxCounter < counters[A[i] - 1]) {
                    maxCounter = counters[A[i] - 1];
                }
            }
        }


        return counters;
    }

    public int[] solution_from_Ruslan(int N, int[] A) {
        if (A == null || A.length == 0) {
            throw new IllegalArgumentException("A has to be a non-empty array");
        }
        if (A.length < 1 || A.length > 100000) {
            throw new IllegalArgumentException("A length has to be within the range [1..100,000]");
        }
        if (N < 1 || N > 100000) {
            throw new IllegalArgumentException("N has to be within the range [1..100,000]");
        }

        int[] counters = new int[N];
        int currentMaxCounter = 0;
        int currentMaxCounterPos = 0;
        int appliedMaxCounter = 0;

        for (int a : A) {
            if (1 <= a && a <= N) {
                ++counters[a - 1];

                if (currentMaxCounter <= counters[a - 1]) {
                    currentMaxCounter = counters[a - 1];
                    currentMaxCounterPos = a - 1;
                }
            } else if (a == N + 1) {
                appliedMaxCounter += currentMaxCounter;
                counters[currentMaxCounterPos] -= appliedMaxCounter;
                counters = new int[N];
                currentMaxCounter = 0;
                currentMaxCounterPos = 0;
            }
        }

        for (int j = 0; j < counters.length; j++) {
            counters[j] += appliedMaxCounter;
        }

        return counters;
    }


}
