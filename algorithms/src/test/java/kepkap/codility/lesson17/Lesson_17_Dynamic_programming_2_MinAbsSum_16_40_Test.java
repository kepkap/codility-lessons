package kepkap.codility.lesson17;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * For a given array A of N integers and a sequence S of N integers from the set {−1, 1}, we define val(A, S) as follows:
 * <p>
 * val(A, S) = |sum{ A[i]*S[i] for i = 0..N−1 }|
 * <p>
 * (Assume that the sum of zero elements equals zero.)
 * <p>
 * For a given array A, we are looking for such a sequence S that minimizes val(A,S).
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given an array A of N integers, computes the minimum value of val(A,S) from all possible values of val(A,S) for all possible sequences S of N integers from the set {−1, 1}.
 * <p>
 * For example, given array:
 * <p>
 * A[0] =  1
 * A[1] =  5
 * A[2] =  2
 * A[3] = -2
 * your function should return 0, since for S = [−1, 1, −1, 1], val(A, S) = 0, which is the minimum possible value.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [0..20,000];
 * each element of array A is an integer within the range [−100..100].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*max(abs(A))2);
 * expected worst-case space complexity is O(N+sum(abs(A))), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 */
public class Lesson_17_Dynamic_programming_2_MinAbsSum_16_40_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{1, 5, 2, -2})).isEqualTo(0);//-1,1-1,1
        assertThat(solution(new int[]{3, 3, 3, 4, 5})).isEqualTo(0);//-1,-1,-1,1,1
        assertThat(solution(new int[]{1, 5, 2, -2, 3})).isEqualTo(1);//1,-1,1,1,-1
        assertThat(solution(new int[]{1, 5, 2, -2, 4})).isEqualTo(0);
        assertThat(solution(new int[]{1, 5, 2, -2, 5})).isEqualTo(1);
        assertThat(solution(new int[]{1, 5, 3, -2, 5})).isEqualTo(0);
        assertThat(solution(new int[]{1, 5, 2, -2, 4, 1})).isEqualTo(1);
        assertThat(solution(new int[]{1, 5, 2, -2, 4, 1, 16})).isEqualTo(1);
        assertThat(solution(new int[]{1, 5, 10, 15, 20, 42})).isEqualTo(1);
        assertThat(solution(new int[]{7})).isEqualTo(7);
    }

    //https://codility.com/media/train/solution-min-abs-sum.pdf
    public int solution(int[] A) {


        int sum = 0;
        int max = 0;
        for (int i = 0; i < A.length; i++) {
            int abs = Math.abs(A[i]);
            sum += abs;
            max = Math.max(max, abs);
        }

        int[] dp = new int[sum + 1];
        dp[0] = 1;

        for (int j = 0; j < A.length; j++) {

            for (int i = sum; i > -1; i--) {
                if (dp[i] == 1 && i + A[j] <= sum) {
                    dp[i + A[j]] = 1;
                }

            }
        }
        int result = sum;

        for (int i = 0; i < sum / 2 + 1; i++) {
            if (dp[i] == 1) {
                result = Math.min(result, sum - 2 * i);
            }

        }

        return result;
    }
}

