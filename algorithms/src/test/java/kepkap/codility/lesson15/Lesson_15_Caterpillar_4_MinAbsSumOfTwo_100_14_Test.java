package kepkap.codility.lesson15;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Let A be a non-empty zero-indexed array consisting of N integers.
 * <p>
 * The abs sum of two for a pair of indices (P, Q) is the absolute value |A[P] + A[Q]|, for 0 ? P ? Q < N.
 * <p>
 * For example, the following array A:
 * <p>
 * A[0] =  1
 * A[1] =  4
 * A[2] = -3
 * has pairs of indices (0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2).
 * The abs sum of two for the pair (0, 0) is A[0] + A[0] = |1 + 1| = 2.
 * The abs sum of two for the pair (0, 1) is A[0] + A[1] = |1 + 4| = 5.
 * The abs sum of two for the pair (0, 2) is A[0] + A[2] = |1 + (?3)| = 2.
 * The abs sum of two for the pair (1, 1) is A[1] + A[1] = |4 + 4| = 8.
 * The abs sum of two for the pair (1, 2) is A[1] + A[2] = |4 + (?3)| = 1.
 * The abs sum of two for the pair (2, 2) is A[2] + A[2] = |(?3) + (?3)| = 6.
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N integers, returns the minimal abs sum of two for any pair of indices in this array.
 * <p>
 * For example, given the following array A:
 * <p>
 * A[0] =  1
 * A[1] =  4
 * A[2] = -3
 * the function should return 1, as explained above.
 * <p>
 * Given array A:
 * <p>
 * A[0] = -8
 * A[1] =  4
 * A[2] =  5
 * A[3] =-10
 * A[4] =  3
 * the function should return |(?8) + 5| = 3.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [?1,000,000,000..1,000,000,000].
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N*log(N));
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 *
 *
 * https://codility.com/demo/results/trainingC4RFXA-384/
 */
public class Lesson_15_Caterpillar_4_MinAbsSumOfTwo_100_14_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{1, 4, -3})).isEqualTo(1);
        assertThat(solution(new int[]{-8, 4, 5, -10, 3})).isEqualTo(3);
    }


    public int solution(int[] A) {
        int back = 0;

        int result = Integer.MAX_VALUE;
        while (back < A.length) {

            int head = back;

            while (head < A.length) {
                result = Math.min(result, Math.abs(A[back] + A[head]));
                head++;
            }
            back++;
        }

        return result;
    }


}

