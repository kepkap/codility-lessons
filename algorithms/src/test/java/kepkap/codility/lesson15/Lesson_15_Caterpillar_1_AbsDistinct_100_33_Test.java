package kepkap.codility.lesson15;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A non-empty zero-indexed array A consisting of N numbers is given.
 * The array is sorted in non-decreasing order.
 * The absolute distinct count of this array is the number of distinct absolute values among the elements of the array.
 * <p>
 * For example, consider array A such that:
 * <p>
 * A[0] = -5
 * A[1] = -3
 * A[2] = -1
 * A[3] =  0
 * A[4] =  3
 * A[5] =  6
 * The absolute distinct count of this array is 5, because there are 5 distinct absolute values among the elements of this array, namely 0, 1, 3, 5 and 6.
 * <p>
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given a non-empty zero-indexed array A consisting of N numbers, returns absolute distinct count of array A.
 * <p>
 * For example, given array A such that:
 * <p>
 * A[0] = -5
 * A[1] = -3
 * A[2] = -1
 * A[3] =  0
 * A[4] =  3
 * A[5] =  6
 * the function should return 5, as explained above.
 * <p>
 * Assume that:
 * <p>
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [?2,147,483,648..2,147,483,647];
 * array A is sorted in non-decreasing order.
 * Complexity:
 * <p>
 * expected worst-case time complexity is O(N);
 * expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 * Elements of input arrays can be modified.
 * <p>
 * https://codility.com/demo/results/trainingHUJAB7-CU6/
 */
public class Lesson_15_Caterpillar_1_AbsDistinct_100_33_Test {


    @Test
    public void test() {
        assertThat(solution(new int[]{-5, -3, -1, 0, 3, 6})).isEqualTo(5);
        assertThat(solution(new int[]{-5, -3, -1, 0, 3, 6, 7, 8})).isEqualTo(7);
    }


    public int solution(int[] A) {
        int count = 1;

        int front = 1;
        while (front < A.length) {
            int back = front - 1;

            while (back > 0 && Math.abs(A[back]) < Math.abs(A[front])) {
                back--;
            }

            if (Math.abs(A[back]) == Math.abs(A[front])) {
                front++;
                continue;
            }
            front++;
            count++;
        }

        return count;
    }


}

